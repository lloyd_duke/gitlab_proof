# gitlab_proof

[Verifying my OpenPGP key: openpgp4fpr:AEDC5D0C7877D5E587692434B540288F0E427C81]

## Getting started
This is an OpenPGP proof that connects my OpenPGP key to this GitLab account. For details check out https://keyoxide.org/guides/openpgp-proofs
